<?php

namespace Core\Controllers;

class PageController {
	private $requestURI;
	private $scriptName;
	private $commands;
	private $viewsDir = 'views';

	private $sections = [
		'content',
		'styles',
		'scripts'
	];

	public function __construct() {
		$this->requestURI = explode('/', $_SERVER['REQUEST_URI']);
		$this->scriptName = explode('/', $_SERVER['SCRIPT_NAME']);
		$this->commands = $this->getCommands();
	}

	public function showPage() {
		if (empty($this->commands[0])) {
			$this->commands[0] = 'index';
		}

		return $this->renderPage($this->getLayout(), $this->getPage());
	}

	private function getCommands() {
		$this->commands = $this->requestURI;

		for ($i = 0; $i < sizeof($this->scriptName); $i++) {
			if ($this->commands[$i] == $this->scriptName[$i]) {
				unset($this->commands[$i]);
			}
		}

		$this->commands = array_values($this->commands);

		return $this->commands;
	}

	private function getLayout() {
		if (!file_exists('app/'.$this->viewsDir.'/layouts/index.php')) {
			die('Layout not found');
		}

		return file_get_contents('app/'.$this->viewsDir.'/layouts/index.php');
	}

	private function getPage() {
		if (!file_exists('app/'.$this->viewsDir.'/pages/'.$this->commands[0].'/index.php')) {
			return $this->get404();
		}

		return file_get_contents('app/'.$this->viewsDir.'/pages/'.$this->commands[0].'/index.php');
	}

	private function get404() {
		if (!file_exists('app/'.$this->viewsDir.'/pages/404/index.php')) {
			die('404 page not found');
		}

		return file_get_contents('app/'.$this->viewsDir.'/pages/404/index.php');
	}

	private function renderPage($layout, $page) {
		foreach ($this->sections as $section) {
			$startSection = '@'.$section;
			$endSection = '@end'.$section;

			$ini = strpos($page, $startSection);
			
			if ($ini !== false) {
				$ini += strlen($startSection);
				$len = strpos($page, $endSection, $ini) - $ini;

				$repl = substr($page, $ini, $len);
			} else {
				$repl = '';
			}

			$layout = str_replace('['.strtoupper($section).']', $repl, $layout);
		}

		return $layout;
	}
}
