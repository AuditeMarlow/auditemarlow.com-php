@styles
<link rel="stylesheet" type="text/css" href="app/public/assets/css/music.css">
@endstyles

@content
<div class="music">
	<h1>Music</h1>

	<div class="playlist">
		<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/245899251&amp;color=8758AB&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
	</div>
</div>
@endcontent
