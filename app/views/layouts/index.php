<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="icon" type="image/png" sizes="32x32" href="app/public/assets/icon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="app/public/assets/icon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="app/public/assets/icon/favicon-16x16.png">
	<link rel="stylesheet" type="text/css" href="app/public/assets/css/menu-top.css">
	[STYLES]
	<title>Audite Marlow</title>
</head>
<body>
	<div class="menu">
		<ul class="list">
			<li><a href="/">Home</a></li>
			<li><a href="/about">About</a></li>
			<li><a href="/music">Music</a></li>
		</ul>
	</div>

	<div class="content">
		[CONTENT]
	</div>

	<div class="footer">
		&copy; Audite Marlow
	</div>
</body>
</html>
